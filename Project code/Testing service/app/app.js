import React from 'react';
import ReactDOM from 'react-dom';

// material-ui
import injectTapEventPlugin from 'react-tap-event-plugin';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import {Tabs, Tab} from 'material-ui/Tabs';
import Paper from 'material-ui/Paper';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import {RadioButton, RadioButtonGroup} from 'material-ui/RadioButton';
import Divider from 'material-ui/Divider';

const styles = {
	headline: {
		fontSize: 24,
		paddingTop: 16,
		marginBottom: 12,
		fontWeight: 400,
	},

	uploadButton: {
		marginTop: 20
	}
};

export default class App extends React.Component {

	constructor(props){
		super(props);

		this.state = {
			value: 'reader',
			reader: {
				file: null,
				mode: "default",
				testMode: "all",
				ip: "130.56.250.75",
				port: "80",
				interval: 1000
			},
			generator: {
				cameraId: 1,
				incrementRate: 0.1,
				start : {
					x: 0,
					y: 0,
					z: 0
				},
				end : {
					x: 0,
					y: 0,
					z: 0
				}
			},
			result: []
		};

		this._handleChange = this._handleChange.bind(this);
		this._onFileChangeHandler = this._onFileChangeHandler.bind(this);
		this._uploadData = this._uploadData.bind(this);
		this._loadFileHandler = this._loadFileHandler.bind(this);
		this._fileErrorHandler = this._fileErrorHandler.bind(this);	
		this._fileGenerator = this._fileGenerator.bind(this);
		this._handleModeChange = this._handleModeChange.bind(this);
		this._handleTestModeChage = this._handleTestModeChage.bind(this);
	}

	componentWillMount(){
		// touch event listener
		injectTapEventPlugin();
	}

	_handleChange(value){
		if(value == "reader" || value == "generator"){
			this.setState({
				value: value,
			});
		}
	};

	_onFileChangeHandler(){
	    if(this.refs.file.files.length > 0){
	    	var state = this.state;
	    	state.reader.file = this.refs.file.files[0]
	      	this.setState({state});
	    }
  	}

	_uploadData(){
		if(this.state.reader.mode === "test"){
			var socket = new WebSocket(`ws://${this.state.reader.ip}:${this.state.reader.port}`);
			var object = {
				[this.state.reader.mode]: {
					testMode: this.state.reader.testMode		
				}
			}

			socket.onopen = () => {
     			socket.send(JSON.stringify(object));
     		}

	     	socket.onmessage = (event) =>{
	     		if (this.state.reader.mode === "test"){
		  			var result = JSON.parse(event.data);
		  			var state = Object.assign({}, this.state);
					state.result.push(result.text)
					this.setState({state});
				}
			}
	
		} else{
			var reader = new FileReader();
			if (this.state.reader.file !== null){
			    // Read file into memory as UTF-8      
			    reader.readAsText(this.state.reader.file);
			    // Handle errors load
			    reader.onload = this._loadFileHandler;
			    reader.onerror = this._fileErrorHandler;
			} else {
				alert("Please choose data set file");
			}
		}
	}

	_loadFileHandler(event) {
    	const csv = event.target.result;
     	const allTextLines = csv.split(/\r\n|\n/);
		var socket = new WebSocket(`ws://${this.state.reader.ip}:${this.state.reader.port}`);
     	
     	var index = 0 ;

     	const dataGenerator = () => {
			setTimeout(() => {     		
      			var data = allTextLines[index].split(",");
      			var object = {}

      			if (this.state.reader.mode === "default"){
      				object = {
      					id: parseInt(data[0]),
      					leftEye: data[1],
      					rightEye: data[2],
	      				headNumber: data[3], // this need to be changed acrroding to a real data
	      				X: data[4],
	      				Y: data[5],
	      				Z: data[6]
      				}
      			} else {
      				object = {
								[this.state.reader.mode]: {
									id: parseInt(data[0]),
			      					leftEye: data[1],
			      					rightEye: data[2],
				      				headNumber: data[3], // this need to be changed acrroding to a real data
				      				X: data[4],
				      				Y: data[5],
				      				Z: data[6]
			      				}
		      				}

      			}
      			
      			socket.send(JSON.stringify(object));
      			index++;

      			if (index < allTextLines.length - 1){
      				dataGenerator();
      			}

  			}, parseInt(this.state.reader.interval));

     	}

     	socket.onopen = () => {
     		dataGenerator()
     	}
   	}

	_fileErrorHandler(evt){
		if(evt.target.error.name == "NotReadableError") {
		  alert("Canno't read file !");
		}
  }

	_fileGenerator(){
		var data = [];

		for (var x = this.state.generator.start.x; x <= this.state.generator.end.x; x += parseFloat(this.state.generator.incrementRate)){
			var data = {
				"x": x.toFixed(4),
				"y": 0,
				"z": 0
			}

			console.log(data);
		}
	}

	_handleModeChange(event, index, value){
		var state = Object.assign({}, this.state);
    	state.reader.mode = value;
    	this.setState({state});
	}

	_handleTestModeChage(event, value){
		var state = Object.assign({}, this.state);
		state.reader.testMode = value;
		this.setState({state});
	}

	render() {
		return (
			<MuiThemeProvider>
				<div>
					<h1>VXLab Kinect Data Reader</h1>
					<Paper className="container" zDepth={2}>
						<Tabs value={this.state.value} onChange={this._handleChange}>
							<Tab label="Reader" value="reader">
						      <div className="tabContent">
						        <SelectField
						          floatingLabelText="Mode"
						          value={this.state.reader.mode}
						          onChange={this._handleModeChange}
						        >
						        	<MenuItem key="test" value="test" primaryText="Test" />
						        	<MenuItem key="calibrate" value="calibrate" primaryText="Calibrate" />
						        	<MenuItem key="configure" value="configure" primaryText="Configure" />
						        	<MenuItem key="default" value="default" primaryText="Default" />
						        </SelectField>
						        {
						        	this.state.reader.mode === "test" ? (
						        		<RadioButtonGroup name="shipSpeed" defaultSelected="all" value={this.state.reader.testMode} onChange={this._handleTestModeChage}>
									      <RadioButton
									        value="all"
									        label="All"
									      />
									      <RadioButton
									        value="connection"
									        label="Connection"
									      />
									      <RadioButton
									        value="k2r"
									        label="K2R"
									      />
									      <RadioButton
									        value="jitter"
									        label="Jitter Removal"
									      />
									      <RadioButton
									        value="main"
									        label="Main Function"
									      />
									    </RadioButtonGroup>
						        	) : false
						        }
						        <br/>
						        {
						        	this.state.reader.mode === "default" ? (
						        		<div>
						        			<br/>
							        		<input type='file' ref='file' onChange={this._onFileChangeHandler} accept=".csv"/>
									        <br/>
									        <TextField
											    hintText="Request Interval (ms)"
											    floatingLabelText="Request Interval (ms)"
											    value={this.state.reader.interval}
											    onChange={(event, index, value) => {
											    	var state = this.state;
											    	state.reader.interval = event.target.value;
											    	this.setState({state});
											    }}
										    />
									    </div>
								    ) : false
								}	
						        <div>
						        	<TextField
									    hintText="Server IP Address"
									    floatingLabelText="Server IP Address"
									    value={this.state.reader.ip}
									    onChange={(event, index, value) => {
									    	var state = this.state;
									    	state.reader.ip =  event.target.value;
									    	this.setState({state});
									    }}
								    />
								    &nbsp;
								    <TextField
									    hintText="Server IP Port"
									    floatingLabelText="Server IP Port"
									    value={this.state.reader.port}
									    onChange={(event, index, value) => {
									    	var state = this.state;
									    	state.reader.port =  event.target.value;
									    	this.setState({state});
									    }}
								    />
								</div>
								<br/>
						        <RaisedButton label="Upload Data" primary={true} style={styles.uploadButton} onClick={this._uploadData}/>
						        {
						        	this.state.result.length > 0 ? (
						        		<div>
						        			<Divider
						        				style={{marginTop: 15}}
						        			/>
						        			{
								        		this.state.result.map((value, index) => {
								        			return (<p key={index}>{value}</p>)
								        		})
							        		}
						        		</div>
						        	) : false
						        }
						        
						      </div>
						    </Tab>
							
						    <Tab label="Generator" value="generator" >
								<div className="tabContent">
									<div>
										<TextField
									    hintText="Camera ID"
									    floatingLabelText="Camera ID"
									    value={this.state.generator.cameraId}
									    onChange={(event, index, value) => {
								    		var state = this.state;
								    		state.generator.cameraId =  event.target.value;
								    		this.setState({state});
								    	}} />
								    	<br/>
								    	<TextField
									    hintText="Incremental Rate"
									    floatingLabelText="Incremental Rate"
									    value={this.state.generator.incrementRate}
									    onChange={(event, index, value) => {
								    		var state = this.state;
								    		state.generator.incrementRate =  event.target.value;
								    		this.setState({state});
								    	}}
										/>
								    </div>
								    <h3 className="sectionHeading">Starting Position</h3>
								    <div className="config-row">
								    	<TextField
									    hintText="X Position"
									    floatingLabelText="X Position"
									    value={this.state.generator.start.x}
									    onChange={(event, index, value) => {
								    		var state = this.state;
								    		state.generator.start.x =  event.target.value;
								    		this.setState({state});
								    	}}
										/>

										<TextField
									    hintText="Y Position"
									    floatingLabelText="Y Position"
									    value={this.state.generator.start.y}
									    onChange={(event, index, value) => {
								    		var state = this.state;
								    		state.generator.start.y =  event.target.value;
								    		this.setState({state});
								    	}}
										/>

										<TextField
									    hintText="Z Position"
									    floatingLabelText="Z Position"
									    value={this.state.generator.start.z}
									    onChange={(event, index, value) => {
								    		var state = this.state;
								    		state.generator.start.z =  event.target.value;
								    		this.setState({state});
								    	}}
										/>
									</div>

									<h3 className="sectionHeading">Ending Position</h3>
								    <div className="config-row">
								    	<TextField
									    hintText="X Position"
									    floatingLabelText="X Position"
									    value={this.state.generator.end.x}
									    onChange={(event, index, value) => {
								    		var state = this.state;
								    		state.generator.end.x =  event.target.value;
								    		this.setState({state});
								    	}}
										/>

										<TextField
									    hintText="Y Position"
									    floatingLabelText="Y Position"
									    value={this.state.generator.end.y}
									    onChange={(event, index, value) => {
								    		var state = this.state;
								    		state.generator.end.y =  event.target.value;
								    		this.setState({state});
								    	}}
										/>

										<TextField
									    hintText="Z Position"
									    floatingLabelText="Z Position"
									    value={this.state.generator.end.z}
									    onChange={(event, index, value) => {
								    		var state = this.state;
								    		state.generator.end.z =  event.target.value;
								    		this.setState({state});
								    	}}
										/>
									</div>

									<RaisedButton label="Generate File" primary={true} style={styles.uploadButton} onClick={this._fileGenerator}/>
								</div>
						    </Tab>
						</Tabs>
					</Paper>
				</div>
			</MuiThemeProvider>
		);
	}
}

const app = document.getElementById('app');
ReactDOM.render(<App />, app);