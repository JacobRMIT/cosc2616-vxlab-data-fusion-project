// ----------------------------------------------------------------------------------------------------
// Variable and constants
// ----------------------------------------------------------------------------------------------------
var lastKinect = 0;
var distanceThreshold = 0.3;

// ----------------------------------------------------------------------------------------------------
// Local functions
// ----------------------------------------------------------------------------------------------------

//Determines if this is the same person based on distance of head markers, only use with positions from two different Kinects
const isClose = (newObj, oldObj) => {
    if (Math.abs(oldObj.X - newObj.X) < distanceThreshold && Math.abs(oldObj.Y - newObj.Y) < distanceThreshold && Math.abs(oldObj.Z - newObj.Z) < distanceThreshold) {
        return true;
    }
    else {
        return false;
    }
}

// ----------------------------------------------------------------------------------------------------
// Export module
// ----------------------------------------------------------------------------------------------------
module.exports = {
    isClose
}
