// ----------------------------------------------------------------------------------------------------
// Node Module Import
// ----------------------------------------------------------------------------------------------------
const geometry = require('./k2r.js')                  // K2R Lib 
	Vector = require('./util.js').Vector,
	distanceVector = require('./util.js').DistanceVector


// ----------------------------------------------------------------------------------------------------
// Variable and constants
// ----------------------------------------------------------------------------------------------------
var lastReading = {};
var lastPos = {};
var bestSensor = {};
var bestSensorSet = {};
// ----------------------------------------------------------------------------------------------------
// Local functions
// ----------------------------------------------------------------------------------------------------

//return wether the data should be accepted
const jitterRemoval = (camId, data) => {
    //take care of the capital and the smal XYZ in the same data structure redudancy
	var xraw = data.X;
	var yraw = data.Y;
	var zraw = data.Z;

	var rawpos = new Vector(xraw, yraw, zraw);
	var newEyePos = geometry.k2r(rawpos, camId);
	var xnew = newEyePos.x;
	var ynew = newEyePos.y;
	var znew = newEyePos.z;

	var pos = new Vector(xnew, ynew, znew);
	if (this.lastPos === null) {
		this.lastPos = pos;
	}

	var dist = Math.abs(distanceVector(lastPos,pos));

	lastPos = new Vector(xnew, ynew, znew);

	if (this.lastReading === undefined) {
	    this.lastReading = {};
	}

	if (this.bestSensor === undefined) {
	    this.bestSensor = {};
	}
	//////////////////////////////
	// IanP, jitter removal calibration: build index from (camera, approx reported position) to best-known-camera;
	// Which sensor should be used at this position (camera id, raw x, z)?
	// described this as "jitter removal" because it makes it possible to ignore readings
	// from all but one preferred sensor (all readings from other sensors at this position are ignored)
	if (camId in this.lastReading) {
		//var moved = !this.lastReading[camId].equals(rawpos);
		//this.text2.innerHTML=this.text2.innerHTML+"<br>dist: "+ dist;
		// moved: true if dist not 0(is moving), false if dist is 0(not moving)
		var moved = (dist !== 0);
		// find the best sensor		
		if (!moved) {// if not moving
			//console.log("calibration");
			var best = null;
			var bestX = 10000; // FIXME: MAXINT
			var bestTheta = 10000; // FIXME: MAX ANGLE??
			var probe;
			// IanP check several candidate readings from different cameras
			// IanP probe set to each camera in turn
			for (probe in this.lastReading) {
				// v: the rawPos of one camera reading
				var v = this.lastReading[probe];
				// x: x value of one camera reading
				var x = Math.abs(v.x);
				//.. theta: the angle from the centre position of the current camera (``probe'')
				//.. atan2: calculate the angle of right angle triangle with side, height as x and z 
				var theta = Math.abs(Math.atan2(v.z,v.x) - Math.PI/2);
				//console.log("sensor "+probe+" theta "+theta);
				//this.text2.innerHTML=this.text2.innerHTML+"<br>theta: "+ theta;
				//.. finding the smallest(best) angle
				if (theta<bestTheta) {
					best = probe;
					bestTheta = theta;
				}
			}
			//this.text2.innerHTML=this.text2.innerHTML+"<br>set best: "+ best;
			var ax = Math.round(rawpos.x * 10)/10;
			var az = Math.round(rawpos.z * 10)/10;
			//this.text2.innerHTML=this.text2.innerHTML+"<br>key: "+ JSON.stringify(key);
	        var key = {probe: camId, x: ax, z: az};
	        this.bestSensor[key] = parseInt(best, 10);
	        console.log(key);
	        this.bestSensorSet[key]=this.lastReading;
			this.lastReading[camId]=rawpos;
		} else {// if is moving, clean lastReading
			this.lastReading = {};
		}
	} else {// if camId not in lastReading, add rawPos to lastReading
		this.lastReading[camId]=rawpos;
	}

    // IanP, Decide whether to reject a position update.
    // IanP, If we know that there is a better camera than the one providing this reading, ignore this reading.
	var ax = Math.round(rawpos.x * 10)/10;
	var az = Math.round(rawpos.z * 10)/10;
	// index is from (camera, observed approx position) to best-known-camera for that position
	var key = {sensor: camId, x: ax, z: az};
    //this.text2.innerHTML=this.text2.innerHTML+"<br>key: "+ JSON.stringify(key);
	if (key in this.bestSensor) {
		var best = this.bestSensor[key];
		var sensors = this.bestSensorSet[key];
		//this.text2.innerHTML=this.text2.innerHTML+"<br>**** best known: "+ best + " " + JSON.stringify(sensors);
		if (camId in sensors && camId !== best) {
			//this.text2.innerHTML=this.text2.innerHTML+"...reject vs "+camId;
			console.log("reject vs ",camId);
			return false;
		}
	}
	return true;
}

module.exports.jitterRemoval = jitterRemoval;