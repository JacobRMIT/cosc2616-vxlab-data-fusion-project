// ----------------------------------------------------------------------------------------------------
// Node Module Import
// ----------------------------------------------------------------------------------------------------
const userPicker = require('./userPicker.js'), // User Picker
    geometry = require('./k2r.js'), // K2R Lib 
    JR = require('./jitterRemoval.js'), // Jitter Removal
    multiKinectHandler = require('./multipleKinectHandler.js'),
    Vector = require('./util.js').Vector,
    UnityData = require('./util.js').UnityData;

// ----------------------------------------------------------------------------------------------------
// Variable and constants
// ----------------------------------------------------------------------------------------------------

//the ID of the user who is being tracked 
//Currently uses 9999 to denote no follow target chosen yet 
//The ID is created by multiplying Kinect ID by 10 and adding headNum
var followUser = 9999;

var userIsFollowed = false,
    headCode = null;


//which kinect is the position data being translated from
//should be sent from the kinect interface
var kinectNumber = 1,
    headNum = 1;

//array of last transmission of each kinect
var kinectList = [];

//array of all people kinects have found
var peopleList = [];

// ----------------------------------------------------------------------------------------------------
    //App - main function
    //runs obj through transformations and compares and updates lists of previously recieved data.
    //Returns data to be broadcast to Unity or other services
// ----------------------------------------------------------------------------------------------------
module.exports.default = (obj) => {
    //transform obj's position data using kinect-to-room script
    const newPos = geometry.k2r(obj, obj.id);

    //run kinect list update function
    updateKinectList(obj);

    //if obj has a headnumber, indicating it's kinect data,
    //then capture info and run functions to check
    if (obj.headNumber != "undefined") {
        headNum = obj.headNumber;
        kinectNumber = obj.id;
        //Code used to uniquely identify kinects and heads
        headCode = kinectNumber * 10 + headNum;
        //if system is not following a participant, choose this head
        //should only occur for the first head detected after service start
        if (followUser == 9999 && !isNaN(headCode)) {
            followUser = headCode;
            console.log("there's no followed target listed so set as " + followUser);
        }
        //change which user is followed:
        //check if eyes are closed - if a single eye is open while another is closed for a certain period of time, swaps that user to 'followed'
        if (obj.leftEye == "Yes" && obj.rightEye == "No" || obj.leftEye == "No" && obj.rightEye == "Yes") {
            //console.log("One eye closed, one eye opened");
            userPicker.increaseSquintCount(headCode);
            if (userPicker.checkHead(headCode) == true) {
                followUser = headCode;
            }
        } else {
            userPicker.decreaseSquintCount(headCode);
        }
    }

    //check if person has other kinect's person which may be the same
    //if so, check to see which is the best location.
    //if not, proceed with code.
    if (checkNeighborsLocations(obj) == true) {
        //Check if we've decided this is the active participant and set this for sending to Unity
        if (headCode == followUser) {
            userIsFollowed = true;
        } else {
            //console.log("headCode of " + headCode + " does not equal followUser code of " + followUser);
            userIsFollowed = false;
        }
        headCode = null;
        //Return transformed data to main class for broadclass.
        return new UnityData(newPos.X, newPos.Y, newPos.Z, kinectNumber, headNum, userIsFollowed.toString());
    }
}

// ----------------------------------------------------------------------------------------------------
// Local functions
// ----------------------------------------------------------------------------------------------------

/* -------------------------------------------------------------------------------------
    Update kinect list:
    Check if new kinect data is already in list of kinects and tracked person IDs.
    If not, add to list. If so, update list by replacing data.
---------------------------------------------------------------------------------------- */
const updateKinectList = (obj) => {
    var inList = false;
    
    //clean old messages from list
    cleanKinectList();

    if (obj.id != undefined && obj.id !== "undefined" && obj.id != "") {
        for (i = 0; i < kinectList.length; i++) {
            if (kinectList[i].id == obj.id && kinectList[i].headNumber == obj.headNumber) {
                kinectList[i] = obj;
                inList = true;

                //tag item with time it was added to list
                kinectList[i].time = (new Date()).getTime();
            }
        }
        if (inList == false) {
            kinectList.push(obj);

            //tag item with time it was added to list
            kinectList[kinectList.length - 1].time = (new Date()).getTime();
        }
    }
}

// const updatePeopleList = (obj) => {

//     cleanPeopleList();
//     if (obj.id != undefined && obj.id !== "undefined" && obj.id != "") {
//         for (i = 0; i < peopleList.length; i++) {
//             for(j = 0; j < peopleList.kinect.length; j++){
//                 if (peopleList[i].kinect[j].id == obj.id && peopleList[i].kinect[j].id == obj.headNumber){
//                     //update list
//                     //then run check of other kinects in this person identity's age
//                         //if younger than 2 seconds, check confidence
//                             //if this has higher confidence, update peopleList[i].current with this

//                 }
//                 else{
//                     //check location of .current vs this, if close then add
//                 }

//             }
//         }
//     }
// }

/* -------------------------------------------------------------------------------------
    Clean kinect list: 
    Go through kinect list and delete anything older than X seconds 
    time is in miliseconds and is after gettime(). Current set to 2 seconds.
---------------------------------------------------------------------------------------- */
const cleanKinectList = () => {

    //var participantHead

    for (i = 0; i < kinectList.length; i++) {
        //get current time
        var currentTime = new Date();
        //if object age is more than X000 microseconds, then run deletion processes 
        if (kinectList[i].time < (currentTime.getTime() - 2000)) {
            //If we're removing the participant, find the closest head and set to participant
            //itterate through array, first look for those closest from other kinects, if none
            //then pick someone from 
            if (kinectList[i].id * 10 + kinectList[i].headNumber == followUser) {
                var leastDistance;
                var chosenHead;
                //variables for use if they're near edge of kinect, assume they're in different kinect
                var chosenHeadDiff;
                var leastDistanceDiff;
                var nearKinectEdge = 0.85;
                for (j = 0; j < kinectList.length; j++) {

                    //if ((kinectList[j].id * 10 + kinectList[j].headNumber) != followUser) {
                    if (getObjID(kinectList[j]) != followUser) {
                        //check distance between new and old locations. 
                        var distanceCheckX = kinectList[j].X - kinectList[i].X;
                        var distanceCheckY = kinectList[j].Y - kinectList[i].Y;
                        var distanceCheckZ = kinectList[j].Z - kinectList[i].Z;
                        var distanceMagnitude = Math.sqrt((distanceCheckX * distanceCheckX) + (distanceCheckY * distanceCheckY) + (distanceCheckZ * distanceCheckZ));
                        //for first run through loop mark first 
                        if (chosenHead == undefined) {
                            //save closest user from different kinect
                            if (kinectList[j].id != followUser.id) {
                                chosenHeadDiff = j;
                                leastDistanceDiff = distanceMagnitude;
                            }
                            leastDistance = distanceMagnitude;
                            chosenHead = j;
                        }

                        else if (distanceMagnitude < leastDistance) {
                            if (kinectList[j].id != followUser.id) {
                                chosenHeadDiff = j;
                                leastDistanceDiff = distanceMagnitude;
                            }
                            chosenHead = j;
                            leastDistance = distanceMagnitude;
                        }
                    }
                }
                //if location was near edge of kinect, search in neighboring kinects before current
                if (Math.abs(followUser.X) > nearKinectEdge && chosenHeadDiff != undefined) {
                    followUser = kinectList[chosenHeadDiff].id * 10 + kinectList[chosenHeadDiff].headNumber;
                }
                else {
                    //check there is another user to change to
                    if(chosenHead != undefined){
                        // update participantHead on caller function
                        followUser = kinectList[chosenHead].id * 10 + kinectList[chosenHead].headNumber;
                    }
                    //else if noone else left in scene, set followUser to non-id
                    else followUser = 9999;
                }
            }
            //object is too old, delete from list
            //following command removes i'th object from kinectList array 
            kinectList.splice(i, 1);
        }
    }
}
/* -------------------------------------------------------------------------------------
    Neighboring kinect check:
    Check if other kinect objs may be same person
------------------------------------------------------------------------------------- */
const checkNeighborsLocations = (obj) => {
    if (obj.id != undefined && obj.id != "undefined" && obj.id != "") {
        for (i = 0; i < kinectList.length; i++) {
            if (kinectList[i].id != obj.id) {
                if (multiKinectHandler.isClose(geometry.k2r(obj), geometry.k2r(kinectList[i])) == true) {

                    //NEED TO MODIFY BEST KINECT ALG TO TAKE 2 OBJS
                    //run jitter/ best kinect algorithm
                    //if this is found to be best kinect, return true
                    //else return false.
                    //HACK: of the two, find the one with the x value closest to zero.
                    if (Math.abs(obj.X) < Math.abs(kinectList[i].X)) {
                        if (followUser == kinectList[i].id * 10 + kinectList[i].headNumber) {
                            followUser = obj.id * 10 + obj.headNumber;
                        }
                        return true;
                    }

                    //Following code called only if old Kinect is the better option
                    //Change the stored followUser to the new head under the new Kinect
                    if (followUser == obj.id * 10 + obj.headNumber) {
                        followUser = kinectList[i].id * 10 + kinectList[i].headNumber;
                    }
                    return false;
                }
                //return true;
            }
        }
    }
    return true;
}

//NEW - 
const getObjID = (obj) => {
  return (obj.id * 10 + obj.headNumber);
}
