// ----------------------------------------------------------------------------------------------------
// Variable and constants
// ----------------------------------------------------------------------------------------------------
var headDictionary = {};
var gestureLength = 10;

// ----------------------------------------------------------------------------------------------------
// Local functions
// ----------------------------------------------------------------------------------------------------
const increaseSquintCount = (headNum) => {
    //If this particular head and kinect has not been added to the dictionary yet, add it
    if (!(headNum in headDictionary)){
        headDictionary.headNum = 0;
    }
    
    if (headDictionary[headNum] < gestureLength) {
        headDictionary[headNum]++;
       // console.log("squint count for head number " + headNum + " is " + headDictionary[headNum]);
    }
    else {
        headDictionary[headNum] = gestureLength;
    }
}

const decreaseSquintCount = (headNum) => {
    if (headDictionary[headNum] > 0) {
        headDictionary[headNum] = headDictionary[headNum] - 2;
        //console.log("squint count for head number " + headNum + " is " + headDictionary[headNum]);
    }
    else {
        headDictionary[headNum] = 0;
    }
}

const checkHead = (headNum) => {
    if (parseInt(headDictionary[headNum]) >= parseInt(gestureLength)) {
        //console.log("gesture recognised");
        return true;
    }
    else {
        //console.log("gesture not recognised, " + headDictionary[headNum] + " is less than " + gestureLength);
        return false;
    }
}


// ----------------------------------------------------------------------------------------------------
// Export module
// ----------------------------------------------------------------------------------------------------
module.exports = {
    increaseSquintCount,
    decreaseSquintCount,
    checkHead
}
