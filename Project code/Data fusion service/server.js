// ----------------------------------------------------------------------------------------------------
// Node Module Import
// ----------------------------------------------------------------------------------------------------
const WebSocketServer = require('ws').Server,                   // websocket
    app = require('./app.js').default,                          // Main App.
    calibrationApp = require('./calibrationApp.js').default,
    test = require('./testSystem.js'),                          // Test Lib.
    util = require('./util.js');                                // utility Lib   

// ----------------------------------------------------------------------------------------------------
// Server Configuration
// ----------------------------------------------------------------------------------------------------
const port = 8087,
    wss = new WebSocketServer({ port: port });

console.log("===============================================================================");
console.info("  Serving WebSocketServer at port", port);
console.info("  MODE: ", process.env.RUNNING_MODE);
console.log("  ------------------------------------");
console.info("  Ctrl+C to stop running server");
console.log("===============================================================================");

// ----------------------------------------------------------------------------------------------------
// Server config
// ----------------------------------------------------------------------------------------------------
wss.on('connection', ws => {

    // on message listener
    ws.on('message', message => {

        const obj = JSON.parse(message);
        /* -------------------------------------------------------------------------------
            Whenever a message is recieved, the server checks,
            the name of the package - if it's 'test' it interperates it as a requrest
            to run an internal test. Otherwise it interperates it as kinect code as per usual
        ------------------------------------------------------------------------------- */
        if (Object.keys(obj)[0] == "test") {
            var testResult = [];
            switch (obj.test.testMode) {
                case "main":
                    testResult.push(test.main());
                    break;
                case "k2r":
                    testResult.push(test.k2r_p1());
                    testResult.push(test.k2r_p2());
                    break;
                case "connection":
                    testResult.push(test.basic());
                case "jitter":
                    testResult.push(test.jitterRemovalTest1());
                    testResult.push(test.jitterRemovalTest2());
                default:
                    testResult.push(test.basic());
                    testResult.push(test.k2r_p1());
                    testResult.push(test.k2r_p2());
                    testResult.push(test.jitterRemovalTest1());
                    testResult.push(test.jitterRemovalTest2());
                    testResult.push(test.main());
                    break;
            }

            testResult.map(value => {
                ws.send(
                    JSON.stringify({
                        text: value
                    })
                );
            });

        }
        else {
            /* -------------------------------------------------------------------------------
                If not running tests,
                Check obj has a kinect ID, if so run k2r on obj's location and save output to
                newPos, for use later if object is broadcast.
            ------------------------------------------------------------------------------- */
            
            if (obj.id != undefined && obj.id !== "undefined" && obj.id != "") { 
                
                if (process.env.RUNNING_MODE === 'calibration'){
                    calibrationApp(obj);
                }else{
                    // main app.
                    const result = app(obj);
                    if (result){
                        wss.clients.forEach(conn => {
                            conn.send(JSON.stringify(result));
                        });
                    }
                }
            }
        }
    });
});
