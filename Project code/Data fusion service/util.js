// ----------------------------------------------------------------------------------------------------
// This file consist of utility functions that will be used across the app
// ----------------------------------------------------------------------------------------------------
function Vector(x, y, z){
    this.X = x;
    this.Y = y;
    this.Z = z;
}

function UnityData(x, y, z, kinectId, headNum, isParticipantString){
    this.x = x;
    this.y = y;
    this.z = z;
    this.id = kinectId;
    this.headNum = headNum;
    this.isFocused = isParticipantString;
}

function DistanceVector( v1, v2 ){
    var dx = v1.x - v2.x;
    var dy = v1.y - v2.y;
    var dz = v1.z - v2.z;

    return Math.sqrt( dx * dx + dy * dy + dz * dz );
}

// ----------------------------------------------------------------------------------------------------
// Export module
// ----------------------------------------------------------------------------------------------------
module.exports = {
	Vector,
	UnityData,
    DistanceVector
}