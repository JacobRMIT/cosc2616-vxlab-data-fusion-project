// ----------------------------------------------------------------------------------------------------
// Node Module Import
// ----------------------------------------------------------------------------------------------------
const app = require('./app.js').default,            // Main App.
    k2r = require('./k2r.js').k2r,                  // K2R Lib
    jr = require('./jitterRemoval.js'),
    Vector = require('./util.js').Vector

// ----------------------------------------------------------------------------------------------------
// Variable and constants
// ----------------------------------------------------------------------------------------------------
var testv3, output, expectedResult;

// ----------------------------------------------------------------------------------------------------
// Local functions
// ----------------------------------------------------------------------------------------------------
const basic = () => {
    return "Basic test - PASSED";
}

const k2r_p1 = () => {
    console.log("Running k2r script test for portal 1");
    testV3 = "test";
    testV3 = new Vector(0, 0, 0);

    output = k2r(testV3, 1)
    expectedResult = new Vector(0, 0, 1);
    if (output.X == expectedResult.X && output.Y == expectedResult.Y && output.Z == expectedResult.Z)
        return "K2R_P1 - PASSED";
    else {
        console.log("returned result:", output);
        console.log("expected result:", expectedResult);
        return "K2R_P1 - FAILED";
    }
}

const k2r_p2 = () => {
    console.log("Running k2r script test for portal 2");
    testV3 = "test";
    testV3 = new Vector(0, 0, 0);

    output = k2r(testV3, 2)
    expectedResult = new Vector(1, 0, 0);
    if (output.X == expectedResult.X && output.Y == expectedResult.Y && output.Z == expectedResult.Z)
        return "K2R_P2 - PASSED";
    else {
        console.log("returned result:", output);
        console.log("expected result:", expectedResult);
        return "K2R_P2 - FAILED";
    }
}

const jitterRemovalTest1 = () => {
    jr.jitterRemoval(1, new Vector(0.6, 0.3, 0.2 ));
    var dropped = jr.jitterRemoval(2, new Vector(0.6, 0.3, 0.3));
    console.log("dropped: " + dropped);

    if (!dropped) {
        return "JitterRemoval Test1 - FAILED";
    }else{
        return "JitterRemoval Test1 - PASSED";
    }
}

//Need to workout on the raise exceptions
const jitterRemovalTest2 = () => {
    jr.jitterRemoval(1, new Vector(0.6, 0.3, 0.2 ));
    var dropped = jr.jitterRemoval(2, new Vector(0.6, 0.3, 0.1));
    console.log("dropped: " + dropped);

    if (dropped) {
        return "JitterRemoval Test2 - FAILED";
    }else{
        return "JitterRemoval Test2 - PASSED";
    }
}

const main = () => {
    // ========================================================================
    // Scenario: Running main test with two person, standing at same position
    // Expected result: A person who close one eye get selected
    // ========================================================================
    var fist_person = {
                        id: 1,
                        headNumber: 6, // this need to be changed acrroding to a real data
                        leftEye: "No",
                        rightEye: "No",
                        X: 0.5,
                        Y: 1.636991,
                        Z: 0.6
                    }

    var second_person = {
                        id: 1,
                        headNumber: 1, // this need to be changed acrroding to a real data
                        leftEye: "Yes",
                        rightEye: "No",
                        X: 0.5,
                        Y: 1.636991,
                        Z: 0.6
                    }

    app(fist_person);
    app(fist_person);
    app(fist_person);
    app(fist_person);
    app(fist_person);
    app(fist_person);
    app(fist_person);
    var result = app(second_person) // second person trigger focus change
    if (result.id == 1 &&  result.headNum == 1 && result.isFocused == 'true'){
        return "Main functions - PASSED"
    }
}

// ----------------------------------------------------------------------------------------------------
// Export module
// ----------------------------------------------------------------------------------------------------
module.exports = {
    basic,
    k2r_p1,
    k2r_p2,
    jitterRemovalTest1,
    jitterRemovalTest2,
    main
}
