function k2r(eyePos, camera, trace) {
  if (camera.pos.X === undefined || camera.pos.Z === undefined || camera.angle === undefined) raise("ill-defined camera " + "eyePos " + JSON.stringify(eyePos) + " camera " + camStr(camera));

  var xraw = eyePos.X;
  var yraw = eyePos.Y;
  var zraw = eyePos.Z;
  var theta = camera.angle;
  var xoff = zraw * Math.sin(theta) + xraw * Math.sin(theta + Math.PI / 2);
  var yoff = 1.0 * yraw; // y position not affected by camera angles
  var zoff = zraw * Math.cos(theta) + xraw * Math.cos(theta + Math.PI / 2);
  // room-space position of eye (relative to center of portals)
  var xnew = 0.0 + camera.pos.X + xoff;
  var ynew = 0.0 + camera.pos.Y + yoff;
  var znew = 0.0 + camera.pos.Z + zoff;
  var result = {
    X: xnew,
    Y: ynew,
    Z: znew
  };
  //if (trace) console.log("k2r",eyePos,JSON.stringify(camera),"=",result);
  return result;
}

function dist2d(p1, p2) {
  var dx = p2.X - p1.X;
  var dz = p2.Z - p1.Z;
  return Math.sqrt(dx * dx + dz * dz);
}


function canAngle(a) {
  var result = a;
  if (a < 0) {
    result = 2 * Math.PI + a;
  }
  if (a > 2 * Math.PI) {
    result = a - 2 * Math.PI;
  }
  return result;
}

function camStr(cam) {
  return "(" + cam.pos.x + "," + cam.pos.z + ") @" + cam.angle / Math.PI + " pi-rad";
}
// triangulate
// NOTE: k_p is not used nor needed
// FIXME: is theta_p needed??
// k, k_p are cameras positions
// theta is the known orientation of camera k
// theta_p is the believed orientation of k_p (not used?)
// p1, p2 are the observed positions of two points (according to camera k)
// p1_p, p2_p are the observed positions of two points (according to camera k_p)
// returns the deduced value of k_p
module.exports.calibrateTri = function(k, k_p, theta, theta_p, p1, p2, p1_p, p2_p, trace) {
  // disallow negative z co-ordinate
  if (p1.Z < 0) {
    raise("pre-fail", "p1.Z<0");
  }
  if (p2.Z < 0) {
    raise("pre-fail", "p2.Z<0");
  }
  if (p1_p.Z < 0) {
    raise("pre-fail", "p1_p.Z<0");
  }
  if (p2_p.Z < 0) {
    raise("pre-fail", "p2_p.Z<0");
  }

  var kc = {
    pos: k,
    angle: theta
  };
  // var kpc = {pos:k_p, angle:theta_p}; 
  // kinect-space co-ordinates of p1 and p2
  var p1a = k2r(p1, kc);
  var p2a = k2r(p2, kc);


  // triangle k_p p1_p p2_p with angles theta, alpha, beta
  var theta_p1 = Math.atan2(p1_p.Z, p1_p.X);

  var theta_p2 = Math.atan2(p2_p.Z, p2_p.X);

  var theta = (theta_p2 - theta_p1);

  if (Math.abs(theta) < 0.06) {
    raise("pre-fail", "theta too small");
  }
  var origin = {
    X: 0,
    Z: 0
  };
  var kp1p = dist2d(origin, p1_p); // rename to kpp1p
  var kp2p = dist2d(origin, p2_p); // rename to kpp2p
  if (kp1p === 0) {
    raise("pre-fail", "first known point is at unknown camera origin");
  }
  if (kp2p === 0) {
    raise("pre-fail", "second known point is at unknown camera origin");
  }
  var p1pp2p = dist2d(p1_p, p2_p);



  // law of sines - doesn't give simple answers for obtuse angles! - won't work if p1'-p2' is zero
  // kp2p/sin alpha = kp1p/sin beta = p1pp2p / sin theta
  // NOTE: floating point errors can push alpha to NaN (?!)
  //..: theta, angle of p1_p-O-p2_p
  //..: value mapped on line p1_p and p2_p/ dist between p1_p and p2_p????????
  //..: alpha, angle of O-p1p-p2p
  var a1 = kp2p * Math.sin(theta) / p1pp2p;

  var alpha = Math.asin(a1);
  //..: beta, angle of O-p2p-p1p
  var b1 = kp1p * Math.sin(theta) / p1pp2p;
  var beta = Math.asin(b1);
  // law of cosines - won't work with degenerate triangle (all sides must be nonzero)
  // alpha2, angle of kp-p2p-p1p??
  var alpha2 = Math.acos((p1pp2p * p1pp2p + kp1p * kp1p - kp2p * kp2p) / (2 * p1pp2p * kp1p));
  var beta2 = Math.acos((p1pp2p * p1pp2p + kp2p * kp2p - kp1p * kp1p) / (2 * p1pp2p * kp2p));


  var alpha_sign = 1;
  if (a1 < 0) alpha_sign = -1; // ??????????
  var beta_sign = 1;
  if (b1 < 0) beta_sign = -1;

  alpha = alpha_sign * alpha2;
  beta = beta_sign * beta2;


  if (alpha === 0) {
    raise("pre-fail", "degenerate triangle (points co-linear), alpha===0");
  }
  if (beta === 0) {
    raise("pre-fail", "degenerate triangle (points co-linear), beta===0");
  }
  var sumtriangle = alpha + beta + theta;
  //if (trace) console.log("Math.sin(theta)",Math.sin(theta));

  if (Math.abs(canAngle(sumtriangle) - Math.PI) > 0.1) {
    raise("inv-fail", "angles do not sum to pi " + alpha + " " + beta + " " + theta);
  }
  // point b on p1_p p2_p such that p1_p b k and p2_p b k are right triangles
  //..: the length of k-b
  var d = (p1pp2p * (Math.sin(alpha) * Math.sin(beta))) / Math.sin(alpha + beta);
  // cos alphab = d/kp2p
  //var alphap = Math.acos(d/kp1p);
  //var betap = Math.acos(d/kp2p);
  //if (trace) console.log("alphap",alphap/Math.PI,"pi-rad","triangle rmdr", (Math.PI - (alpha + alphap))/Math.PI,"pi-rad");
  //if (trace) console.log("betap",betap/Math.PI,"pi-rad","triangle rmdr", (Math.PI - (alpha + alphap))/Math.PI,"pi-rad");
  var p1pb = kp1p * Math.cos(alpha);
  var p2pb = kp2p * Math.cos(beta);

  // gamma is angle p1p2 makes wrt z===0
  var dz = 0.0 + p2.Z - p1.Z;
  var dx = 0.0 + p2.X - p1.X;
  var gamma = Math.atan2(dz, dx);


  // find "normal"
  // find point on line segment p1p2 (in frame of reference of k) which is "normal" to k' on line
  //..: looks like we should using p1 p2 rather than p1a p2a
  //var b = {X: 0.0 + p1a.X + p1pb * (p2a.X - p1a.X) / p1pp2p, Z: 0.0 + p1a.Z + p1pb * (p2a.Z - p1a.Z) / p1pp2p};
  var b = {
    X: 0.0 + p1.X + p1pb * (p2.X - p1.X) / p1pp2p,
    Z: 0.0 + p1.Z + p1pb * (p2.Z - p1.Z) / p1pp2p
  };

  var norm = canAngle(gamma - Math.PI / 2);

  // position of k_p in frame of reference of k (project along norm from b for distance d)
  var k_p_ks = {
    X: b.X - d * Math.cos(norm),
    Z: b.Z - d * Math.sin(norm)
  }; //..: why use d in k's frame???

  var k_p_new = k2r(k_p_ks, kc);

  var p1_theta = canAngle(gamma + alpha);

  var true_theta_p1 = canAngle(p1_theta - Math.PI);

  theta_p = (theta_p1 - true_theta_p1);
  // old method
  var result = {
    angle: canAngle(theta_p),
    pos: k_p_new
  };

  return result;
}