// ----------------------------------------------------------------------------------------------------
// Node Module Import
// ----------------------------------------------------------------------------------------------------
const Vector = require('./util.js').Vector;

// ----------------------------------------------------------------------------------------------------
// Variable and constants
// ----------------------------------------------------------------------------------------------------
var startX = 0;
var startY = -2.0;
var startZ = 1.0;

const portals_square = {
    6: {
      pa: new Vector(-1.05,  0.89, -2.0),
      pb: new Vector(-1.05,  0.89, -0.1),
      pc: new Vector(-1.05,  1.93, -2.0)
    },
    1: {
      pa: new Vector(-0.93,  0.89,  0.0),
      pb: new Vector( 0.93,  0.89,  0.0),
      pc: new Vector(-0.93,  1.93, -0.05)
    },
    2: {
      pa: new Vector( 0.98,  0.89, -0.1),
      pb: new Vector( 0.98,  0.89, -2.0),
      pc: new Vector( 1.05,  1.93, -0.1)
    },
    3: {
      pa: new Vector( 0.93,  0.89, -2.09),
      pb: new Vector(-0.93,  0.89, -2.09),
      pc: new Vector( 0.93,  1.93, -2.14)
    },
}

    // Y offsets (height) are floor offsets
    // Generally kinects should be able to provide consistent absolute height
    // assuming LEVEL floor --- should normally adjust only if your ''floor'' is not level!
const kinects_square = {
    // room-space position (relative to kinect 1)
    // angle anticlockwise from the direction camera 1 points (radians)
    // vis: what portals can ''see'' user that this kinect can see?
	// "real 6"
    //6: {
    //        pos: new Vector(-1.2, 0.0, -0.9),
    //        angle: -0.5 * Math.PI,
    //        vis: [3,6,1]
    //},
    1: {
            pos: new Vector(0.0,  0.0,  0.0),
            angle: 0.0,
            vis: [6,1,2],
			onPortal: 1,
    },
    2: {
            pos: new Vector(1.0, 0.0, -1.0),
            angle: 0.5 * Math.PI,
            vis: [1,2,3],
			onPortal: 2,
    },
    3: {
            pos: new Vector(1.8, 0.0, -1.0),
            angle: 1.0 * Math.PI,
            vis: [2,3,6]
    },
    6: {
            pos: new Vector(0.8, 0.0, 0.0),
            angle: 0.16 * Math.PI,
            vis: [1,2]
    },
    7: {
            pos: new Vector(1.0, 0.0, -0.2),
            angle: 0.33 * Math.PI,
            vis: [1,2]
    }
}


// ----------------------------------------------------------------------------------------------------
// Local functions
// ----------------------------------------------------------------------------------------------------
const k2r = (eyePos, cameraNumber) => {
    if (cameraNumber === undefined){
      cameraNumber = eyePos.id;
    }

    var camera = kinects_square[cameraNumber];
    var xraw = eyePos.X;
    var yraw = eyePos.Y;
    var zraw = eyePos.Z;

    //console.log("k2r has received X: " + xraw + " Y: " + yraw + " Z: " + zraw);

    var theta = camera.angle;
    var xoff = zraw * Math.sin(theta) + xraw * Math.sin(theta+Math.PI/2);
    var yoff = 1.0 * yraw; // y position not affected by camera angles
    var zoff = zraw * Math.cos(theta) + xraw * Math.cos(theta+Math.PI/2);
    //this.text2.innerHTML=this.text2.innerHTML+"<br>k2r camera: "+ JSON.stringify(camera);
    // room-space position of eye (relative to center of portals)
    //console.log("k2r camera ",JSON.stringify(camera));
    var xnew = 0.0 + startX + camera.pos.X + xoff;
    var ynew = 0.0 + camera.pos.Y + yoff;
    var znew = 0.0 + startZ + camera.pos.Z + zoff;

    return new Vector(xnew, ynew, znew);
};

const kinects = (n) => {
	return kinects_square[n];
}

// ----------------------------------------------------------------------------------------------------
// Export module
// ----------------------------------------------------------------------------------------------------
module.exports = {
    k2r,
    kinects
}