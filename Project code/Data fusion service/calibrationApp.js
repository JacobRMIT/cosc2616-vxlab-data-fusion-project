
var lastReadings = {},
	lastReadingTime,
	lastReadingCmr,
	objGot = false,
	objCount = 0,
	TIMEPASS = 0.3;

module.exports.default = (obj) => {

	var currentReadingCmr = obj.id;

	if (lastReadingTime) {
		timeSeg = lastReadingTime - new Date();
	}
	//if did's got an object
	if (!objGot && obj) {
		console.log("Start getting position 1");
		if (currentReadingCmr in lastReadings) {
			console.log("cameraID in last readings");
			var dist = Math.abs(dist2d(lastReadings[currentReadingCmr][0], obj));
			if (dist >= 0.4) {
				lastReadings[currentReadingCmr].push(obj);
				objGot = true;
				console.log("Got position 1 ");
			} else {
				console.log("objs too closed, dist: " + dist);
			}
		} else {
			console.log("camea ID NOT in last readings");
			lastReadings[currentReadingCmr] = [obj];
			objGot = true;
			console.log("Got position 1 ");
		}



	}
	//if got an object from different camera and in a short time range, see it as the same object from another camera
	else if (objGot && obj && currentReadingCmr != lastReadingCmr && timeSeg <= TIMEPASS) {

		if (currentReadingCmr in lastReadings) {
			lastReadings[currentReadingCmr].push(obj);
		} else {
			lastReadings[currentReadingCmr] = [obj];
		}
		objCount++;
		objGot = false; // finish getting this object
		console.log("Got position 2");
		console.log("Got Object :" + objCount)


	}
	//if already got an object from one camera again, resign last reading for the camera
	if (objGot && currentReadingCmr == lastReadingCmr) {
		var lan = lastReadings[currentReadingCmr].length;
		lastReadings[currentReadingCmr][lan - 1] = obj;
		console.log("reassigned last reading from camera");
	}

	if (lastReadings.length >= 2 && !(currentReadingCmr in lastReadings)) {
		console.log("more than 2 kinects - SHOULD NOT HAPPEN");
		objCount = 0;
		objGot = false;
		lastReadings = [];
		lastReadingCmr = "";
	}


	if (calibrate && objCount == 2) {

		var camIds = Object.keys(lastReadings);
		console.log("lastReadings: " + JSON.stringify(lastReadings));
		console.log("camIds: " + JSON.stringify(camIds));

		var k1 = geometry.kinects(camIds[0]);
		var k2 = geometry.kinects(camIds[1]);

		//console.log("camera 1: " + JSON.stringify(k1));
		//console.log("camera 2: " + JSON.stringify(k2));

		var k1p1 = lastReadings[camIds[0]][0];
		var k1p2 = lastReadings[camIds[0]][1];

		//console.log("k1p1 & k1p2:");
		//console.log(k1p1);
		//console.log(k1p2);

		var k2p1 = lastReadings[camIds[1]][0];
		var k2p2 = lastReadings[camIds[1]][1];

		//console.log("k2p1 & k2p2:");
		//console.log(k2p1);
		//console.log(k2p2);

		console.log("Start Calibration");

		var new_k2_pos = cali.calibrateTri(k1.pos, k2.pos, k1.angle, k2.angle, k1p1, k1p2, k2p1, k2p2, true);

		console.log("Got new position for kinect " + camIds[1], "new Pos: " + JSON.stringify(new_k2_pos.pos) + "angle: " + new_k2_pos.angle);

		// after calibration, clean lastReadings
		objCount = 0;
		objGot = false;
		lastReadings = [];
		lastReadingCmr = [];
	}


	lastReadingCmr = obj.id;
	lastReadingTime = new Date();
}