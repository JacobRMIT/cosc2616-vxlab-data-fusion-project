﻿//#define DEBUG

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERVICE
{
    /**
     * Lightweight object around websocket, handles string and binary communication
     *
     * @module server
     * @submodule WebsocketIO
     * @requires ws
     */

    using System;
    using WebSocket4Net;
    using System.Collections.Generic;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;
    using System.Threading;
    using System.Diagnostics;

    // see http://stackoverflow.com/questions/6416017/json-net-deserializing-nested-dictionaries/6417753
    class MyConverter : CustomCreationConverter<IDictionary<string, object>>
    {
        public override IDictionary<string, object> Create(Type objectType)
        {
            return new Dictionary<string, object>();
        }

        public override bool CanConvert(Type objectType)
        {
            // in addition to handling IDictionary<string, object>
            // we want to handle the deserialization of dict value
            // which is of type object
            return objectType == typeof(object) || base.CanConvert(objectType);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.StartObject
                || reader.TokenType == JsonToken.Null)
                return base.ReadJson(reader, objectType, existingValue, serializer);

            // if the next token is not an object
            // then fall back on standard deserializer (strings, numbers etc.)
            return serializer.Deserialize(reader);
        }

        public static Object readjson(String json)
        {
            var obj = JsonConvert.DeserializeObject<IDictionary<string, object>>(
                json, new JsonConverter[] { new MyConverter() });
            return obj;
        }
    }

    /**
     * Client socket object
     *
     * @class WebsocketIO
     * @constructor
     * @param ws {Object} ULR of the server or actual websocket
     * @param strictSSL {Bool} require or not SSL verification with a certiifcate
     * @param openCallback {Function} callback when the socket opens
     */
    public class WebsocketIO
    {
        WebSocket ws;
        //Dictionary<String, Callback> messages;
        int aliasCount;
        Dictionary<String, String> remoteListeners;
        Dictionary<String, String> localListeners;
        String wsname;

        private void debug(string s)
        {
#if (DEBUG)
            Debug.WriteLine(s);
#endif
        }

        public WebsocketIO(String wsname)
        { 
            debug("New WSIO...");
            this.wsname = wsname;
            reset();
        }

        void reset()
        {
            this.ws = new WebSocket(wsname, "", WebSocketVersion.None);

            //this.messages = new Dictionary<String, Callback>();

            this.aliasCount = 1;
            this.remoteListeners = new Dictionary<String, String>();
            this.remoteListeners["#WSIO#addListener"] = "0000";
            this.localListeners = new Dictionary<String, String>();
            this.localListeners["0000"] = "#WSIO#addListener";

            ws.Opened += onOpen;
            //ws.Error += onError;
            //ws.Closed += new EventHandler(onClosed);
            //ws.MessageReceived += onMessage;
            debug("opening WS...");
            ws.Open();
            ws.Send("OPEN!");
        }

        //public void onError(object sender, EventArgs e)
        //{
        //    debug("WSIO error " + e.ToString());
        //    // avoid swamping the network/stack
        //    Thread.Sleep(3000);
        //    reset();
        //}

        public void onOpen(object sender, EventArgs e)
        {
            debug("WSIO Connected...");
            //openCallback.callback(this);
            //this.ws.binaryType = "arraybuffer";
            //this.remoteAddress = {address: _this.ws._socket.remoteAddress, port: _this.ws._socket.remotePort};
            //this.id = _this.remoteAddress.address + ":" + _this.remoteAddress.port;
        }

        //public void onMessage(object sender, MessageReceivedEventArgs m)
        //{
        //    debug("WSIO received message " + m.Message.ToString());
        //    Dictionary<String, Object> r = (Dictionary<String, Object>)MyConverter.readjson(m.Message);

        //    String fName;
            
        //    //if (typeof message === "string") {
        //    String f = (String)r["f"];
        //    Dictionary<String, Object> d = (Dictionary<String, Object>)r["d"];

        //    debug("- f: " + f);

        //    fName = this.localListeners[f];
        //    //if (fName == undefined)
        //    //{
        //    //    console.log("WebsocketIO>\tno handler for message");
        //    //}
        //    // add lister to client
        //    //else
        //    if (fName == "#WSIO#addListener")
        //    {
        //        String listener = (String)d["listener"];
        //        String alias = (String)d["alias"];
        //        debug("addListener: " + listener + " " + alias);
        //        this.remoteListeners[listener] = alias;
        //        return;
        //    }
        //    // handle message
        //    else
        //    {
        //        this.messages[fName].callback(this, d);
        //    }
        //    //}
        //    //else {
        //    //	var func  = String.fromCharCode(message[0]) +
        //    //				String.fromCharCode(message[1]) +
        //    //				String.fromCharCode(message[2]) +
        //    //				String.fromCharCode(message[3]);
        //    //	fName = _this.localListeners[func];
        //    //	var buf = message.slice(4, message.length);
        //    //	_this.messages[fName](_this, buf);
        //    //}
        //}

        //public interface OpenCallback
        //{
        //    void callback(WebsocketIO wsio);
        //}

        //public interface Callback
        //{
        //    void callback(WebsocketIO wsio, Dictionary<String, Object> d);
        //}

        /**
        * Set a message handler for a given name
        *
        * @method on
        * @param name {String} name for the handler
        * @param callback {Function} handler to be called for a given name
        */
        //public void on(String name, Callback callback)
        //{
        //    // pad with leading zeros to length 4
        //    String alias = this.aliasCount.ToString("X4");
        //    //this.localListeners[alias] = name;
        //    //this.messages[name] = callback;
        //    //this.aliasCount++;
        //    //String message = new JavaScriptSerializer().Serialize(obj);
        //    String message = "{\"listener\": \"" + name + "\", \"alias\": \"" + alias + "\" }";
        //    String addListenerAlias = "#WSIO#addListener";
        //    this.emitString(addListenerAlias, message);
        //}

        public bool emit(String name, Object data)
        {
            String message = JsonConvert.SerializeObject(data);
            return emitString(name, message);
        }

        public class Msg
        {
            public String f;
            public Object d;
        }

        public bool emitString(String name, String dataString)
        {
            //debug("emitting string!");
            try
            {
                //if (!this.remoteListeners.ContainsKey(name))
                //{
                //    debug("Remote does not yet advertise support for " + name);
                //}
                String alias = "TEST ALIAS";
                Msg m = new Msg();
                m.f = alias;
                m.d = dataString;

                //String message = JsonConvert.SerializeObject(m);
                //               "{\"f\":\"" + alias + "\",\"d\":" + dataString + "}"
                //String message = " {    \"f\" : \"" + alias + "\" , \"d\" : " + dataString + " } ";
                String message = dataString;

                //debug("WSIO.emitString: " + message);
                this.ws.Send(message);
                //Console.WriteLine("EMITTING MESSAGE");
                //String message2 = "{ \"user\" : \"bob\", \"text\", \"hello\" }";
                //this.ws.Send(message2);
                //}
                return true;
            }
            catch
            {
                return false;
            }
        }

    }

}