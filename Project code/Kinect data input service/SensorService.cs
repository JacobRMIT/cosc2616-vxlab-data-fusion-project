﻿//#define DEBUG

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SERVICE
{
    public class SensorService
    {
        /* USAGE:            
         */

        private bool connecting = false;
        private SERVICE.WebsocketIO wsio;

        //Number of messages to ignore in order to reduce load on server
        public float ditchNum = 4;
        public float ditchCount = 0;

        public class Requests
        {
            public String config = "True";
            public String version = "False";
            public String time = "False";
            public String console = "False";
        }

        public class AddClientInfo
        {
            public String clientType = "vxlabTracker";
            public Requests requests;
        }

        String hostport;

        public SensorService(String hostport)
        {
            //this.hostport = "ws://127.0.0.1:8087";
            this.hostport = hostport;
            //this.hostport = "ws://130.56.250.75:80/"; ;
        }

        public SensorService()
        {
            //this.hostport = "ws://131.170.250.74:8901/";
            //this.hostport = "ws://10.234.2.22:8901/";
            Console.WriteLine("Setting IP");
            //this.hostport = "ws://10.234.2.28:8087";
            this.hostport = "ws://127.0.0.1:8087";
        }

        private void debug(string s)
        {
#if (DEBUG)
            Debug.WriteLine(s);
#endif
        }

        public void connect() {
            connecting = true;
            debug("New WSIO...");
            this.wsio = new SERVICE.WebsocketIO(hostport);
            Thread.Sleep(1000);
            debug("WSIO Open...?");
            /* Adapt from SAGE2 pointer protocol...
            wsio.on('initialize', initialize)
	        wsio.on('setupDisplayConfiguration', setupDisplayConfiguration)
	        wsio.emit('addClient', {'clientType': "sageUI", 'requests': {'config': True, 'version': False, 'time': False, 'console': False}})
	        wsio.emit('registerInteractionClient', {'name': "PyPointer", 'color': "#B4B4B4"})            */
            //wsio.on("initialize", new InitCallback());
            //wsio.emitString("addClient", "{ \"clientType\" : \"vxlabTracker\", \"requests\": {\"config\": \"True\", \"version\": \"False\", \"time\": \"False\", \"console\": \"False\"} }");
            AddClientInfo ai = new AddClientInfo();
            Requests rq = new Requests();
            ai.requests = rq;
            wsio.emit("addClient", ai);
            Thread.Sleep(2000);
            debug("NOT SAGE");
            wsio.emitString("registerInteractionClient", "{ \"name\" : \"vxlabTrackerClient\" }");
            connecting = false;
        }

        private void send(String name, String dataString)
        {
            if (connecting)
            {
                Console.WriteLine("still connecting");
                return;
            }
            if (!wsio.emitString(name, dataString))
            {
                connect();
            }
        }



        //// x, y, z are in metres (offset from kinect)
        //// yaw, pitch, roll are in degrees(?)
        //// rename to: headTrack
        public void sendHead(ulong bodyID, float X, float Y, float Z, int yaw, int pitch, int roll, Microsoft.Kinect.Face.FaceFrameResult faceResult, int bodiesEligible, int headNumber)
        //public void sendHead(ulong bodyID, float X, float Y, float Z, int yaw, int pitch, int roll, Microsoft.Kinect.Face.FaceFrameResult faceResult, int bodiesEligible)
        {
            if (ditchCount == ditchNum)
            {
                String leftEyeClosed = faceResult.FaceProperties[Microsoft.Kinect.Face.FaceProperty.LeftEyeClosed].ToString();
                String rightEyeClosed = faceResult.FaceProperties[Microsoft.Kinect.Face.FaceProperty.RightEyeClosed].ToString();
                String wearingGlasses = faceResult.FaceProperties[Microsoft.Kinect.Face.FaceProperty.WearingGlasses].ToString();

                String engaged = faceResult.FaceProperties[Microsoft.Kinect.Face.FaceProperty.Engaged].ToString();


                long now = (long)DateTime.UtcNow.ToUniversalTime().Subtract(
                                new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)
                                 ).TotalMilliseconds;

                //debug("1," + X + "," + Y + "," + Z + "," + now);

                //Old then then - added headNumber id to 
                //send("headPosition", "{\"id\": \"2\", \"X\": " + X + ", \"Y\": \"" + Y + "\", \"Z\": \"" + Z + "\"}");
                send("headPosition", "{\"id\": 1, \"leftEye\":\"" + leftEyeClosed + "\", \"rightEye\":\"" + rightEyeClosed + "\", \"headNumber\": \"" + headNumber + "\", \"X\": " + X + ", \"Y\": \"" + Y + "\", \"Z\": \"" + Z + "\"}");
                /*send("headPosition", "{ \"data\" : { \"BodyID\": \"" + bodyID.ToString()
                    + "\",\"X\": " + X + ", \"Y\": \"" + Y + "\", \"Z\": \"" + Z
                    + "\", \"yaw\": \"" + yaw + "\", \"pitch\": \"" + pitch + "\", \"roll\": \"" + roll
                    + "\", \"leftEyeClosed\": \"" + leftEyeClosed
                    + "\", \"rightEyeClosed\": \"" + rightEyeClosed
                    + "\", \"wearingGlasses\": \"" + wearingGlasses
                    + "\", \"engaged\": \"" + engaged
                    + "\", \"bodies\": \"" + bodiesEligible
                    + "\" } }"
                    );*/
                ditchCount = 0;
           }
            else
            {
                ditchCount++;
           }
        }

        //public void sendNobody()
        //{
        //    send("headPosition", "{ \"data\" : { \"BodyID\": \"0\" } }");
        //}

        //public void sendThrow(string type, string direction)
        //{
        //    send("throwApp", "{ \"data\" : { \"type\": \"" + type + "\", \"direction\": \"" + direction + "\" } }");
        //}

        //class InitCallback : SERVICE.WebsocketIO.Callback
        //{
        //    public void callback(WebsocketIO wsio, Dictionary<string, object> d)
        //    {
        //        Console.WriteLine("SAGE2 WSIO initialize (user callback)");
        //    }

        //}
    }
}
