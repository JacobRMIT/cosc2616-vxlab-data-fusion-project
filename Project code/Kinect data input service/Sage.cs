﻿//#define DEBUG

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERVICE
{
    // SAGE2 is available for use under the SAGE2 Software License
    //
    // University of Illinois at Chicago's Electronic Visualization Laboratory (EVL)
    // and University of Hawai'i at Manoa's Laboratory for Advanced Visualization and
    // Applications (LAVA)
    //
    // See full text, terms and conditions in the LICENSE.txt included file
    //
    // Copyright (c) 2014

    /**
     * Lightweight object around websocket, handles string and binary communication
     *
     * @module server
     * @submodule WebsocketIO
     * @requires ws
     */

    using System;
    using WebSocket4Net;
    using System.Collections.Generic;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;
    using System.Threading;

    public class Sage
    {
        /* USAGE:
            SAGE2.Sage sage = new SAGE2.Sage();
            sage.connect();
            sage.sendHead("10.234.2.71", 1.5f, 2, 3, 4, 5, 6);
         */

        private bool connecting = false;
        private SERVICE.WebsocketIO wsio;

        public class Requests
        {
            public String config = "True";
            public String version = "False";
            public String time = "False";
            public String console = "False";
        }

        public class AddClientInfo
        {
            public String clientType = "vxlabTracker";
            public Requests requests;
        }

        String hostport;

        public Sage(String hostport)
        {
            this.hostport = hostport;
        }

        public Sage()
        {
            
            //this.hostport = "ws://131.170.250.74:8901/";
            //this.hostport = "ws://10.234.2.22:8901/";
            this.hostport = "ws://10.234.2.28:8901/";
        }

        private void debug(string s)
        {
#if (DEBUG)
            Console.WriteLine(s);
#endif
        }

        public void connect() {
            connecting = true;
            debug("New WSIO...");
            this.wsio = new SERVICE.WebsocketIO(hostport);
            Thread.Sleep(1000);
            debug("WSIO Open...?");
            /* Adapt from SAGE2 pointer protocol...
            wsio.on('initialize', initialize)
	        wsio.on('setupDisplayConfiguration', setupDisplayConfiguration)
	        wsio.emit('addClient', {'clientType': "sageUI", 'requests': {'config': True, 'version': False, 'time': False, 'console': False}})
	        wsio.emit('registerInteractionClient', {'name': "PyPointer", 'color': "#B4B4B4"})            */
            //wsio.on("initialize", new InitCallback());
            //wsio.emitString("addClient", "{ \"clientType\" : \"vxlabTracker\", \"requests\": {\"config\": \"True\", \"version\": \"False\", \"time\": \"False\", \"console\": \"False\"} }");
            AddClientInfo ai = new AddClientInfo();
            Requests rq = new Requests();
            ai.requests = rq;
            wsio.emit("addClient", ai);
            Thread.Sleep(2000);
            debug("SAGE");
            wsio.emitString("registerInteractionClient", "{ \"name\" : \"vxlabTrackerClient\" }");
            connecting = false;
        }

        private void send(String name, String dataString)
        {
            if (connecting)
            {
                Console.WriteLine("still connecting");
                return;
            }
            if (!wsio.emitString(name, dataString))
            {
                connect();
            }
        }

        // x, y, z are in metres (offset from kinect)
        // yaw, pitch, roll are in degrees(?)
        // rename to: headTrack
        public void sendHead(ulong bodyID, float X, float Y, float Z, int yaw, int pitch, int roll, Microsoft.Kinect.Face.FaceFrameResult faceResult, int bodiesEligible) {
            debug("TRYING TO SEND HEAD");
            String leftEyeClosed = faceResult.FaceProperties[Microsoft.Kinect.Face.FaceProperty.LeftEyeClosed].ToString();
            String rightEyeClosed = faceResult.FaceProperties[Microsoft.Kinect.Face.FaceProperty.RightEyeClosed].ToString();
            String wearingGlasses= faceResult.FaceProperties[Microsoft.Kinect.Face.FaceProperty.WearingGlasses].ToString();

            String engaged = faceResult.FaceProperties[Microsoft.Kinect.Face.FaceProperty.Engaged].ToString();
            wsio.emitString("headPosition", "{ \"X\": " + X + ", \"Y\": \"" + Y + "\", \"Z\": \"" + Z + "\"}");

            /*wsio.emitString("headPosition", "{ \"data\" : { \"BodyID\": \"" + bodyID.ToString()
                + "\",\"X\": " + X + ", \"Y\": \"" + Y + "\", \"Z\": \"" + Z
                + "\", \"yaw\": \"" + yaw + "\", \"pitch\": \"" + pitch + "\", \"roll\": \"" + roll
                + "\", \"leftEyeClosed\": \"" + leftEyeClosed
                + "\", \"rightEyeClosed\": \"" + rightEyeClosed
                + "\", \"wearingGlasses\": \"" + wearingGlasses
                + "\", \"engaged\": \"" + engaged
                + "\", \"bodies\": \"" + bodiesEligible
                + "\" } }"
                );*/
        }

        public void sendNobody()
        {
            send("headPosition", "{ \"data\" : { \"BodyID\": \"0\" } }");
        }

        public void sendThrow(string type, string direction)
        {
            send("throwApp", "{ \"data\" : { \"type\": \"" + type + "\", \"direction\": \"" + direction + "\" } }");
        }

        //class InitCallback : SERVICE.WebsocketIO.Callback
        //{
        //    public void callback(WebsocketIO wsio, Dictionary<string, object> d)
        //    {
        //        Console.WriteLine("SAGE2 WSIO initialize (user callback)");
        //    }

        //}

        /*
        public static class UserOpenCallback : OpenCallback
        {
            public void callback(WebsocketIO wsio)
            {
                Console.WriteLine("User: WSIO open callback...");

                String clientDescription = "{ \"clientType\" : \"headTracker\" }";
                //wsio.emitString("addClient", clientDescription);

                wsio.on("initialize", new InitCallback());
            }
        }
         */

        //public static void Main(string[] args)
        //{
        //    Console.WriteLine("Press any key to continue...");
        //    Console.ReadKey();
        //}
    }

}